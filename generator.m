function A = generator(sec, npoints)
%% generator.m
%  sec - secuence of amplitudes
%  npoint -- number of points per amplitude in "sec"

n = length(sec);
temp = ones(1,npoints);
k = 0;
A = [];
for i = 1:n
    A(k+1:k+npoints) = sec(i)*temp;
    k = k + npoints;
end

end
